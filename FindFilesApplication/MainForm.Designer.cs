﻿namespace FindFilesApplication
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxResult1 = new System.Windows.Forms.TextBox();
            this.textBoxResult2 = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonOpenDirectory1 = new System.Windows.Forms.Button();
            this.buttonOpenDirectory2 = new System.Windows.Forms.Button();
            this.labelOpenDirectory1 = new System.Windows.Forms.Label();
            this.labelOpenDirectory2 = new System.Windows.Forms.Label();
            this.labelDirectory1 = new System.Windows.Forms.Label();
            this.labelDirectory2 = new System.Windows.Forms.Label();
            this.labelCountDirectories1 = new System.Windows.Forms.Label();
            this.labelCountDirectories2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxResult1
            // 
            this.textBoxResult1.Location = new System.Drawing.Point(21, 163);
            this.textBoxResult1.Multiline = true;
            this.textBoxResult1.Name = "textBoxResult1";
            this.textBoxResult1.Size = new System.Drawing.Size(245, 236);
            this.textBoxResult1.TabIndex = 0;
            // 
            // textBoxResult2
            // 
            this.textBoxResult2.Location = new System.Drawing.Point(293, 163);
            this.textBoxResult2.Multiline = true;
            this.textBoxResult2.Name = "textBoxResult2";
            this.textBoxResult2.Size = new System.Drawing.Size(240, 236);
            this.textBoxResult2.TabIndex = 1;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Enabled = false;
            this.buttonSearch.Location = new System.Drawing.Point(21, 99);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(128, 23);
            this.buttonSearch.TabIndex = 2;
            this.buttonSearch.Text = "Get all directories";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonOpenDirectory1
            // 
            this.buttonOpenDirectory1.Location = new System.Drawing.Point(21, 37);
            this.buttonOpenDirectory1.Name = "buttonOpenDirectory1";
            this.buttonOpenDirectory1.Size = new System.Drawing.Size(128, 31);
            this.buttonOpenDirectory1.TabIndex = 3;
            this.buttonOpenDirectory1.Text = "Browse first directory";
            this.buttonOpenDirectory1.UseVisualStyleBackColor = true;
            this.buttonOpenDirectory1.Click += new System.EventHandler(this.buttonOpenDirectory1_Click);
            // 
            // buttonOpenDirectory2
            // 
            this.buttonOpenDirectory2.Location = new System.Drawing.Point(244, 37);
            this.buttonOpenDirectory2.Name = "buttonOpenDirectory2";
            this.buttonOpenDirectory2.Size = new System.Drawing.Size(142, 31);
            this.buttonOpenDirectory2.TabIndex = 4;
            this.buttonOpenDirectory2.Text = "Browse second directory";
            this.buttonOpenDirectory2.UseVisualStyleBackColor = true;
            this.buttonOpenDirectory2.Click += new System.EventHandler(this.buttonOpenDirectory2_Click);
            // 
            // labelOpenDirectory1
            // 
            this.labelOpenDirectory1.AutoSize = true;
            this.labelOpenDirectory1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOpenDirectory1.ForeColor = System.Drawing.Color.Red;
            this.labelOpenDirectory1.Location = new System.Drawing.Point(155, 46);
            this.labelOpenDirectory1.Name = "labelOpenDirectory1";
            this.labelOpenDirectory1.Size = new System.Drawing.Size(13, 16);
            this.labelOpenDirectory1.TabIndex = 5;
            this.labelOpenDirectory1.Text = "*";
            // 
            // labelOpenDirectory2
            // 
            this.labelOpenDirectory2.AutoSize = true;
            this.labelOpenDirectory2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelOpenDirectory2.ForeColor = System.Drawing.Color.Red;
            this.labelOpenDirectory2.Location = new System.Drawing.Point(392, 44);
            this.labelOpenDirectory2.Name = "labelOpenDirectory2";
            this.labelOpenDirectory2.Size = new System.Drawing.Size(13, 16);
            this.labelOpenDirectory2.TabIndex = 6;
            this.labelOpenDirectory2.Text = "*";
            // 
            // labelDirectory1
            // 
            this.labelDirectory1.AutoSize = true;
            this.labelDirectory1.Location = new System.Drawing.Point(21, 144);
            this.labelDirectory1.Name = "labelDirectory1";
            this.labelDirectory1.Size = new System.Drawing.Size(0, 13);
            this.labelDirectory1.TabIndex = 7;
            // 
            // labelDirectory2
            // 
            this.labelDirectory2.AutoSize = true;
            this.labelDirectory2.Location = new System.Drawing.Point(290, 144);
            this.labelDirectory2.Name = "labelDirectory2";
            this.labelDirectory2.Size = new System.Drawing.Size(0, 13);
            this.labelDirectory2.TabIndex = 8;
            // 
            // labelCountDirectories1
            // 
            this.labelCountDirectories1.AutoSize = true;
            this.labelCountDirectories1.Location = new System.Drawing.Point(21, 402);
            this.labelCountDirectories1.Name = "labelCountDirectories1";
            this.labelCountDirectories1.Size = new System.Drawing.Size(112, 13);
            this.labelCountDirectories1.TabIndex = 9;
            this.labelCountDirectories1.Text = "Amount of directories: ";
            // 
            // labelCountDirectories2
            // 
            this.labelCountDirectories2.AutoSize = true;
            this.labelCountDirectories2.Location = new System.Drawing.Point(290, 402);
            this.labelCountDirectories2.Name = "labelCountDirectories2";
            this.labelCountDirectories2.Size = new System.Drawing.Size(112, 13);
            this.labelCountDirectories2.TabIndex = 10;
            this.labelCountDirectories2.Text = "Amount of directories: ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 447);
            this.Controls.Add(this.labelCountDirectories2);
            this.Controls.Add(this.labelCountDirectories1);
            this.Controls.Add(this.labelDirectory2);
            this.Controls.Add(this.labelDirectory1);
            this.Controls.Add(this.labelOpenDirectory2);
            this.Controls.Add(this.labelOpenDirectory1);
            this.Controls.Add(this.buttonOpenDirectory2);
            this.Controls.Add(this.buttonOpenDirectory1);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxResult2);
            this.Controls.Add(this.textBoxResult1);
            this.Name = "MainForm";
            this.Text = "Find Directories";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxResult1;
        private System.Windows.Forms.TextBox textBoxResult2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonOpenDirectory1;
        private System.Windows.Forms.Button buttonOpenDirectory2;
        private System.Windows.Forms.Label labelOpenDirectory1;
        private System.Windows.Forms.Label labelOpenDirectory2;
        private System.Windows.Forms.Label labelDirectory1;
        private System.Windows.Forms.Label labelDirectory2;
        private System.Windows.Forms.Label labelCountDirectories1;
        private System.Windows.Forms.Label labelCountDirectories2;
    }
}

