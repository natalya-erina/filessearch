﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindFilesApplication
{
    class MainThread
    {
        private Thread thread;
        private Finder finder1, finder2;
        private TextBox textBox1, textBox2;
        private Label label1, label2;
        public string result1, result2;

        public Thread CurrentThread { get { return thread; } }

        public MainThread(Finder finder1, Finder finder2, 
            TextBox textBox1, TextBox textBox2,
            Label label1, Label label2)
        {
            this.finder1 = finder1;
            this.finder2 = finder2;
            thread = new Thread(new ThreadStart(Find));
            this.textBox1 = textBox1;
            this.textBox2 = textBox2;
            this.label1 = label1;
            this.label2 = label2;
        }

        public void Find()
        {
            finder1.StartWork();
            finder2.StartWork();
            finder1.CurrentThread.Join();
            finder2.CurrentThread.Join();
            
            finder1.GetDirectories(textBox1);
            finder2.GetDirectories(textBox2);

            finder1.GetCount(label1);
            finder2.GetCount(label2);
        }

        public void StartWork()
        {
            thread.Start();
        }
    }
}
