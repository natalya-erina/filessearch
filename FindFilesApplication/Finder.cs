﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

//Заданы два каталога. Сравнить, в каком из них больше вложенных каталогов и вывести все.

namespace FindFilesApplication
{
    class Finder
    {
        private string path;
        private IEnumerable<string> directories;
        private Thread thread;

        public Thread CurrentThread { get { return thread; } }

        public Finder(string path)
        {
            this.path = path;
            thread = new Thread(new ThreadStart(FindDirectories));
        }

        public void GetCount(Label label)
        {
            int count = directories.Count();
            if (label.InvokeRequired)
                label.Invoke(new Action<int>((s) => label.Text += s), count);
            else
                label.Text += count;
        }

        public void GetDirectories(TextBox textBox)
        {
            string result = "";
            foreach (string d in directories)
            {
                result += d + Environment.NewLine;
            }
            if (textBox.InvokeRequired)
                textBox.Invoke(new Action<string>((s) => textBox.AppendText(s)), result);
            else
                textBox.AppendText(result);
        }
        
        public void FindDirectories()
        {
            directories = RsdnDirectory.GetAllDirectories(path);
        }

        public void StartWork()
        {
            thread.Start();
        }
    }
}
