﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindFilesApplication
{
    public partial class MainForm : Form
    {
        private Finder finder1, finder2;
        private bool firstOpened, secondOpened;
        MainThread mainThread;

        public MainForm()
        {
            InitializeComponent();
            firstOpened = secondOpened = false;
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            mainThread = new MainThread(finder1, finder2, textBoxResult1, textBoxResult2,
                labelCountDirectories1, labelCountDirectories2);
            mainThread.StartWork();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mainThread != null && mainThread.CurrentThread.IsAlive)
                mainThread.CurrentThread.Abort();
        }

        private void buttonOpenDirectory1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                finder1 = new Finder(fbd.SelectedPath);
                labelOpenDirectory1.Text = "\u2714";
                labelOpenDirectory1.ForeColor = Color.Green;
                firstOpened = true;
                labelDirectory1.Text = fbd.SelectedPath;
            }

            if (firstOpened && secondOpened)
                buttonSearch.Enabled = true;
        }

        private void buttonOpenDirectory2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                finder2 = new Finder(fbd.SelectedPath);
                labelOpenDirectory2.Text = "\u2714";
                labelOpenDirectory2.ForeColor = Color.Green;
                secondOpened = true;
                labelDirectory2.Text = fbd.SelectedPath;
            }

            if (firstOpened && secondOpened)
                buttonSearch.Enabled = true;
        }
    }
}
